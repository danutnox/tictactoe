"""Salut!"""

import random


print("""Salut!
Ca sa jucam X si 0, va rog sa introduceti cifra aferenta casutei din tabel, atunci cand va vine randul.
Spor!""")
piese = ("X", "0")
alegere = random.choice(piese)


def gameBoard(table):
    board = ""
    for x in range(0, 9, 3):
        board = board + table[x] + " | " + table[x + 1] + " | " + table[x + 2] + "\n"
    return board


# Piesa primului jucator
def player1():
    player_1 = alegere
    return player_1


# Piesa celui de-al doilea jucator
def player2():
    if player1() == "X":
        player_2 = "0"
    else:
        player_2 = "X"
    return player_2


# Cine incepe
def incepe():
    if player1() == "X":
        print("Incepe player 1\n")
        return player1()
    else:
        print("Incepe player 2\n")
        return player2()


print("\nPlayer 1 joaca cu:  " + player1())
print("\nplayer 2 joaca cu:  " + player2())


# Inputul primului jucator
def input1(tabla):
    try:
        user1 = int(input("\nPlayer 1 introdu nr: "))
        if 0 < user1 < 10 and (tabla[user1 - 1]) == "_":
            tabla[user1 - 1] = player1()
        elif user1 < 1 or user1 > 9:
            print("\nNumarul nu se afla in casuta! Incearca din nou")
            input1(tabla)
        else:
            print("\nLocatia este ocupata! Alege alta pozitie!")
            input1(tabla)
    except ValueError:
        print("\nNu ai introdus o cifra!")
        input1(tabla)


# Inputul jucatorului 2
def input2(tabla):
    try:
        user2 = int(input("\nPlayer 2 introdu nr: "))
        if 0 < user2 < 10 and (tabla[user2 - 1]) == "_":
            tabla[user2 - 1] = player2()
        elif user2 < 1 or user2 > 9:
            print("\nNumarul nu se afla in casuta! Incearca din nou")
            input2(tabla)
        else:
            print("\nLocatia este ocupata! Alege alta pozitie!")
            input2(tabla)
    except ValueError:
        print("\nNu ai introdus o cifra!")
        input2(tabla)


def startAgain():
    intrebare = input("\nDoriti sa jucati din nou? y/n  ")
    if intrebare.lower() == "y":
        porneste_jocul()
    elif intrebare.lower() == "n":
        quit()
    else:
        print("\nVa rog introduceti y sau n ")
        startAgain()


def checkWin1(tabla, x, y, z):
    if tabla[x] == tabla[y] == tabla[z] != "_":
        print("\nCastigator este player 1")
        startAgain()

    elif "_" not in tabla:
        print("\nRemiza")
        startAgain()


def win1(tabla):
    checkWin1(tabla, 0, 1, 2)
    checkWin1(tabla, 3, 4, 5)
    checkWin1(tabla, 6, 7, 8)
    checkWin1(tabla, 0, 3, 6)
    checkWin1(tabla, 1, 4, 7)
    checkWin1(tabla, 2, 5, 8)
    checkWin1(tabla, 0, 4, 8)
    checkWin1(tabla, 2, 4, 6)


def win2(tabla):
    checkWin2(tabla, 0, 1, 2)
    checkWin2(tabla, 3, 4, 5)
    checkWin2(tabla, 6, 7, 8)
    checkWin2(tabla, 0, 3, 6)
    checkWin2(tabla, 1, 4, 7)
    checkWin2(tabla, 2, 5, 8)
    checkWin2(tabla, 0, 4, 8)
    checkWin2(tabla, 2, 4, 6)


def checkWin2(tabla, x, y, z):
    if tabla[x] == tabla[y] == tabla[z] != "_":
        print("\nCastigator este player 2")
        startAgain()

    elif "_" not in tabla:
        print("\nRemiza")
        startAgain()


# Functia de incepere joc
def porneste_jocul():
    tabla = ["_", "_", "_", "_", "_", "_", "_", "_", "_"]

    while True:
        if incepe() == player1():
            input1(tabla)
            print(gameBoard(tabla))
            win1(tabla)

            input2(tabla)
            print(gameBoard(tabla))
            win2(tabla)

        else:
            input2(tabla)
            print(gameBoard(tabla))
            win2(tabla)

            input1(tabla)
            print(gameBoard(tabla))
            win1(tabla)


porneste_jocul()